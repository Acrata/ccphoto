<?php
/**
 * Customizer sections.
 *
 * @package CCPhotography
 */

/**
 * Register the section sections.
 *
 * @param object $wp_customize Instance of WP_Customize_Class.
 */
function ccphoto_customize_sections( $wp_customize ) {

	// Register additional scripts section.
	$wp_customize->add_section(
		'ccphoto_additional_scripts_section',
		array(
			'title'    => esc_html__( 'Additional Scripts', 'ccphoto' ),
			'priority' => 10,
			'panel'    => 'site-options',
		)
	);

	// Register a social links section.
	$wp_customize->add_section(
		'ccphoto_social_links_section',
		array(
			'title'       => esc_html__( 'Social Media', 'ccphoto' ),
			'description' => esc_html__( 'Links here power the display_social_network_links() template tag.', 'ccphoto' ),
			'priority'    => 90,
			'panel'       => 'site-options',
		)
	);

	// Register a header section.
	$wp_customize->add_section(
		'ccphoto_header_section',
		array(
			'title'    => esc_html__( 'Header Customizations', 'ccphoto' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);

	// Register a footer section.
	$wp_customize->add_section(
		'ccphoto_footer_section',
		array(
			'title'    => esc_html__( 'Footer Customizations', 'ccphoto' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);
}
add_action( 'customize_register', 'ccphoto_customize_sections' );
