<?php
/**
 * Customizer panels.
 *
 * @package CCPhotography
 */

/**
 * Add a custom panels to attach sections too.
 *
 * @param object $wp_customize Instance of WP_Customize_Class.
 */
function ccphoto_customize_panels( $wp_customize ) {

	// Register a new panel.
	$wp_customize->add_panel(
		'site-options', array(
			'priority'       => 10,
			'capability'     => 'edit_theme_options',
			'theme_supports' => '',
			'title'          => esc_html__( 'Site Options', 'ccphoto' ),
			'description'    => esc_html__( 'Other theme options.', 'ccphoto' ),
		)
	);
}
add_action( 'customize_register', 'ccphoto_customize_panels' );
