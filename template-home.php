<?php
/**
 * Template Name: Home Page
 *
 * The template for displaying pages with ACF components.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package CCPhotography
 */

get_header(); ?>

	<div class="content-area">
		<main id="main" class="site-main">
		<?php 	get_template_part( 'template-parts/content/layout', 'header' ); ?>

					<?php
					while ( have_posts() ) :
					the_post();
					the_content();

						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;

					endwhile; // End of the loop.
					?>
		</main><!-- #main -->
	</div><!-- .primary -->

<?php get_footer(); ?>
